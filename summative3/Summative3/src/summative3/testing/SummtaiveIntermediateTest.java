package summative3.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import summative3.model.SummtaiveIntermediate;

class SummtaiveIntermediateTest {

	@Test
	@DisplayName("Angka Semua")
	public void mainTest() {
		String[] args = { "1", "2", "3", "4" };
		SummtaiveIntermediate.main(args);
		assertEquals(Integer.parseInt(args[0]), 1);

	}
	
	@Test
	@DisplayName(" Huruf semua")
	public void illegalArg() {
		assertThrows(NumberFormatException.class, () -> {
			SummtaiveIntermediate.main(new String[] { "F", "B", "D", "A" });
		});
	}

	@Test
	@DisplayName("Input angka dan huruf")
	public void throwException() {
		assertThrows(NumberFormatException.class, () -> {
			SummtaiveIntermediate.main(new String[] { "F", "c", "2", "3" });
		});
	}

	

}
