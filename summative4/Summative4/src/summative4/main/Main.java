package summative4.main;

import java.util.ArrayList;
import java.util.LinkedList;

import summative4.model.Lift;
import summative4.model.Passenger;

public class Main {
	static LinkedList<Integer> lift = new LinkedList();

	public static void main(String[] args) {
		Lift lantai = new Lift();
		lift.addFirst(0);
		lift.add(1);
		lift.add(2);
		lift.add(3);
		lift.add(4);
		ArrayList<Passenger> passengers = new ArrayList<>();
		Passenger p1 = new Passenger(0, 1);
		passengers.add(p1);
		p1 = new Passenger(0, 4);
		passengers.add(p1);
		lantai.setLantai(4);
		jalan(passengers, lantai);
		System.out.println();
		passengers = new ArrayList<>();
		p1 = new Passenger(2, 0);
		passengers.add(p1);
		p1 = new Passenger(4, 0);
		passengers.add(p1);
		lantai.setLantai(0);
		jalan(passengers, lantai);
		System.out.println();
		passengers = new ArrayList<>();
		p1 = new Passenger(2, 4);
		passengers.add(p1);
		p1 = new Passenger(1, 4);
		passengers.add(p1);
		p1 = new Passenger(4, 1);
		passengers.add(p1);
		lantai.setLantai(3);
		jalan(passengers,lantai);
		
		

	}

	public static void jalan(ArrayList<Passenger> passengers, Lift lantai) {
		int posisi = lantai.getLantai();
		for (int i = 0; i < passengers.size(); i++) {
			for (int j = 0; j < passengers.size() - 1; j++) {
				if (passengers.get(j).getJarak() < passengers.get(i).getJarak()) {
					Passenger p = passengers.get(i);
					passengers.set(i, passengers.get(j));
					passengers.set(j, p);
				}
			}
		}


		System.out.println("Start : LT " + posisi);
		int i = 0;
		lantai.setPenumpang(0);
		while (true) {
			if (passengers.size() != lantai.getPenumpang()) {
				if (lantai.getLantai() == lift.get(0)) {
					if (lantai.getLantai() == passengers.get(i).getStart()) {
						lantai.setPenumpang((lantai.getPenumpang() + 1));
						System.out.println("LT " + lantai.getLantai() + " getCustomer");
						i++;

					} else if (lantai.getLantai() < passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() + 1);
						System.out.println("LT " + lantai.getLantai());
					} else if (lantai.getLantai() > passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() - 1);
						System.out.println("LT " + lantai.getLantai());
					}
				} else if (lantai.getLantai() == lift.get(1)) {
					if (lantai.getLantai() == passengers.get(i).getStart()) {
						lantai.setPenumpang((lantai.getPenumpang() + 1));
						System.out.println("LT " + lantai.getLantai() + " getCustomer");
						i++;

					} else if (lantai.getLantai() < passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() + 1);
						System.out.println("LT " + lantai.getLantai());
					} else if (lantai.getLantai() > passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() - 1);
						System.out.println("LT " + lantai.getLantai());
					}
				} else if (lantai.getLantai() == lift.get(2)) {
					if (lantai.getLantai() == passengers.get(i).getStart()) {
						lantai.setPenumpang((lantai.getPenumpang() + 1));
						System.out.println("LT " + lantai.getLantai() + " getCustomer");
						i++;

					} else if (lantai.getLantai() < passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() + 1);
						System.out.println("LT " + lantai.getLantai());
					} else if (lantai.getLantai() > passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() - 1);
						System.out.println("LT " + lantai.getLantai());
					}
				} else if (lantai.getLantai() == lift.get(3)) {
					if (lantai.getLantai() == passengers.get(i).getStart()) {
						lantai.setPenumpang((lantai.getPenumpang() + 1));
						System.out.println("LT " + lantai.getLantai() + " getCustomer");
						i++;

					} else if (lantai.getLantai() < passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() + 1);
						System.out.println("LT " + lantai.getLantai());
					} else if (lantai.getLantai() > passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() - 1);
						System.out.println("LT " + lantai.getLantai());
					}
				} else if (lantai.getLantai() == lift.get(4)) {
					if (lantai.getLantai() == passengers.get(i).getStart()) {
						lantai.setPenumpang((lantai.getPenumpang() + 1));
						System.out.println("LT " + lantai.getLantai() + " getCustomer");
						i++;

					} else if (lantai.getLantai() < passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() + 1);
						System.out.println("LT " + lantai.getLantai());
					} else if (lantai.getLantai() > passengers.get(i).getStart()) {
						lantai.setLantai(lantai.getLantai() - 1);
						System.out.println("LT " + lantai.getLantai());
					}
				}

			} else {
				break;

			}

			System.out.println();

		}
		for (int k = 0; k < passengers.size(); k++) {
			for (int j = 0; j < passengers.size() - 1; j++) {
				if (Math.abs(passengers.get(j).getEnd() - lantai.getLantai())>Math.abs(passengers.get(k).getEnd()-lantai.getLantai())) {
					Passenger p = passengers.get(k);
					passengers.set(k, passengers.get(j));
					passengers.set(j, p);
				}
			}
		}
		
		i = 0;
		while (true) {
			if (lantai.getLantai() == passengers.get(i).getEnd()) {
				lantai.setPenumpang(lantai.getPenumpang() - 1);
				System.out.println("LT " + lantai.getLantai() + " Drop Customer");
				i++;
			} else if (lantai.getLantai() < passengers.get(i).getEnd()) {
				lantai.setLantai(lantai.getLantai() + 1);
				System.out.println("LT " + lantai.getLantai());
			} else {
				lantai.setLantai(lantai.getLantai() - 1);
				System.out.println("LT " + lantai.getLantai());
			}

			System.out.println();
			if (lantai.getPenumpang() == 0) {
				break;
			}
		}

	}

}
