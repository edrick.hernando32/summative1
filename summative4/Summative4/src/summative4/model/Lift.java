package summative4.model;


public class Lift {
	private int lantai;
	private int penumpang;

	public int getLantai() {
		return lantai;
	}

	public void setLantai(int lantai) {
		this.lantai = lantai;
	}

	public int getPenumpang() {
		return penumpang;
	}

	public void setPenumpang(int penumpang) {
		this.penumpang = penumpang;
	}

}
