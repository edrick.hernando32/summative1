package summative4.model;

public class Passenger {
	private int start;
	private int end;
	private int jarak;

	public Passenger(int start, int end) {
		this.start = start;
		this.end = end;
		this.jarak = Math.abs(end - start);
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getJarak() {
		return jarak;
	}

	public void setJarak(int jarak) {
		this.jarak = jarak;
	}

}
