package summative1.main;

import java.util.ArrayList;

import summative1.model.TraficLight;

public class Main {

	public static void main(String[] args) {
		ArrayList<TraficLight> lamps = new ArrayList<>();
		TraficLight lamp=new TraficLight("Hijau",0,120,1);//lampu posisi jarum jam 12
		lamps.add(lamp);
		lamp=new TraficLight("Merah",240,0,2);//lampu posisi jarum jam 3
		lamps.add(lamp);
		lamp=new TraficLight("Merah",360,0,3);//lampu posisi jarum jam 6
		lamps.add(lamp);
		lamp=new TraficLight("Merah",480,0,4);//lampu posisi jarum jam 9
		lamps.add(lamp);
		lamps.get(0).start();
		lamps.get(1).start();
		lamps.get(2).start();
		lamps.get(3).start();

	}

}
