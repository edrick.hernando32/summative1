package summative2.main;

import java.util.ArrayList;

import summative2.model.*;

public class Main {

	public static void main(String[] args) {
		ArrayList<Parkir> park = new ArrayList<>();
		Mobil mobil = new Mobil("B 1111 CK", 30, 5000, 4500);
		Motor motor = new Motor("B 2222 YK", 30, 4000, 3500);
		park.add(mobil);
		park.add(motor);
		mobil = new Mobil("B 3333 CK", 62, 5000, 4500);
		motor = new Motor("B 4444 YK", 62, 4000, 3500);
		park.add(mobil);
		park.add(motor);
		mobil = new Mobil("B 5555 CK", 	240, 5000, 4500);
		motor = new Motor("B 6666 YK", 240, 4000, 3500);
		park.add(mobil);
		park.add(motor);
		mobil = new Mobil("B 7777 CK", 419, 5000, 4500);
		motor = new Motor("B 8888 YK", 419, 4000, 3500);
		park.add(mobil);
		park.add(motor);
		mobil = new Mobil("B 9999 CK", 2400, 5000, 4500);
		motor = new Motor("B 0000 YK", 2400, 4000, 3500);
		park.add(mobil);
		park.add(motor);
		
		for(int i=0;i<park.size();i++) {
			park.get(i).setTotalHarga(park.get(i).hitungBiayaParkir());
			if(park.get(i) instanceof Mobil) {
				System.out.println("Biaya Parkir Mobil "+park.get(i).getPlat()+" selama "+park.get(i).getDurasi()+" menit sebesar : "+park.get(i).getTotalHarga());
			}else {
				System.out.println("Biaya Parkir Motor "+park.get(i).getPlat()+" selama "+park.get(i).getDurasi()+" menit sebesar : "+park.get(i).getTotalHarga());
				
			}
		}

	}

}
