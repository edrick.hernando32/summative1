package summative2.model;

public class Parkir {
	protected String plat;
	protected double durasi;
	protected int harga;
	protected int hargaPerjam;
	protected double totalHarga;

	public Parkir(String plat, double durasi, int harga, int hargaPerjam) {
		this.plat = plat;
		this.durasi = durasi;
		this.harga = harga;
		this.hargaPerjam = hargaPerjam;
	}

	public String getPlat() {
		return plat;
	}

	public void setPlat(String plat) {
		this.plat = plat;
	}

	public double getDurasi() {
		return durasi;
	}

	public void setDurasi(double durasi) {
		this.durasi = durasi;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public int getHargaPerjam() {
		return hargaPerjam;
	}

	public void setHargaPerjam(int hargaPerjam) {
		this.hargaPerjam = hargaPerjam;
	}

	public double getTotalHarga() {
		return totalHarga;
	}

	public void setTotalHarga(double totalHarga) {
		this.totalHarga = totalHarga;
	}

	public double hitungBiayaParkir() {
		double durasi = getDurasi()/60;
		durasi=Math.ceil(durasi);
		double total = 0;
		if (durasi > 1) {
			durasi = durasi - 1;
			total = getHarga() + (durasi * getHargaPerjam());
		} else {
			total = getHarga();
		}
		return total;
	}

}
