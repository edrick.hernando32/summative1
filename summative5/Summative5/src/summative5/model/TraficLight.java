package summative5.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class TraficLight extends Thread {

	private String colorLight;
	private int time;
	private int greenTime;
	private int position;
	private int capacity;

	public TraficLight() {

	}

	public TraficLight(String colorLight, int time, int greenTime, int position, int capacity) {
		this.colorLight = colorLight;
		this.time = time;
		this.greenTime = greenTime;
		this.position = position;
		this.capacity = capacity;

	}

	public String getColorLight() {
		return colorLight;
	}

	public void setColorLight(String colorLight) {
		this.colorLight = colorLight;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getGreenTime() {
		return greenTime;
	}

	public void setGreenTime(int greenTime) {
		this.greenTime = greenTime;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public void changeColor() {
		ArrayList<String> kata = new ArrayList<>();
		System.out.println("Lampu ke - " + getPosition());
		kata.add("Lampu ke - " + getPosition());
		while (true) {
			if (getColorLight().equalsIgnoreCase("Merah ") || getColorLight().equalsIgnoreCase("Oranye")) {
				System.out.println("Lampu " + getColorLight() + " ke " + getPosition() + " detik ke - " + getTime());

				kata.add("Lampu " + getColorLight() + " ke " + getPosition() + " detik ke - " + getTime());
			}
			if (getTime() > 10) {
				setColorLight("Merah ");
				setTime(getTime() - 1);
			} else if (getTime() <= 10 && getTime() >= 1) {
				setColorLight("Oranye");
				setTime(getTime() - 1);
			} else if (getTime() < 1) {
				setColorLight("Hijau ");
				if (getGreenTime() == 0 || getGreenTime() < 0 || getGreenTime() == 30) {
					setGreenTime(getGreenTime() + 120);
				}
				setTime(0);
				System.out.println("Lampu Menjadi Hijau");
				kata.add("Lampu Menjadi Hijau");
				while (true) {
					if (getGreenTime() > 0) {
						kata.add("Lampu " + getColorLight() + " ke " + getPosition() + " detik ke - " + getGreenTime());
						System.out.println(
								"Lampu " + getColorLight() + " ke " + getPosition() + " detik ke - " + getGreenTime());
						setGreenTime(getGreenTime() - 1);
					} else {
						System.out.println("Lampu Menjadi Merah");
						kata.add("Lampu Menjadi Merah");
						System.out.println();
						setColorLight("Merah");
						setGreenTime(0);
						setTime(getTime() + 480);
						break;
					}

					System.out.println();
					try {
						sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				break;
			}
			System.out.println();
			try {
				sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
		simpan(kata);

	}

	public void changeColorJammed(ArrayList<TraficLight> lamps) {
		ArrayList<TraficLight> jammed = new ArrayList<>();
		ArrayList<TraficLight> unJammed = new ArrayList<>();
		for (int i = 0; i < lamps.size(); i++) {
			if (lamps.get(i).getCapacity() > (200 * 30 / 100)) {
				jammed.add(lamps.get(i));
			} else {
				unJammed.add(lamps.get(i));
			}
		}
		if (jammed.size() >= 3) {
			System.out.println("Lebih dari 2 Jalur tidak sepi!! Tidak ada solusi!");
		} else {
			for (int i = 0; i < jammed.size(); i++) {
				jammed.get(i).setGreenTime(jammed.get(i).getGreenTime() + 30);
				jammed.get(i).setTime(jammed.get(i).getTime() - 30);
				unJammed.get(i).setGreenTime(unJammed.get(i).getGreenTime() - 30);
				unJammed.get(i).setTime(unJammed.get(i).getTime() + 30);
			}
		}

	}

	public void simpan(ArrayList<String> kata) {
		String path = "C:\\Users\\Nitro 5\\eclipse-workspace\\Summative5\\src\\summative5\\model\\traffic-light2.txt";
		File file = new File(path);
		FileWriter isiText;
		boolean res;
		try {
			res = file.createNewFile();
			if (res) {
				isiText = new FileWriter(path);
				for (String isi : kata) {
					isiText.write(isi + "\r\n");
				}
				isiText.close();
			} else {
				file = new File(path);
				Scanner reader = new Scanner(file);
				ArrayList<String> data = new ArrayList<>();
				while (reader.hasNextLine()) {
					data.add(reader.nextLine());
				}
				reader.close();
				isiText = new FileWriter(path);
				for (int j = 0; j < data.size(); j++) {
					isiText.write(data.get(j) + "\r\n");
				}
				for (String isi : kata) {
					isiText.write(isi + "\r\n");
				}
				isiText.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {

		changeColor();

	}

}
