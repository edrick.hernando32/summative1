package summative5.main;

import java.util.ArrayList;

import summative5.model.TraficLight;

public class Main {

	public static void main(String[] args) {
		ArrayList<TraficLight> lamps = new ArrayList<>();
		int[] capacity = new int[3];
		for (int i = 0; i < 3; i++) {
			capacity[i] = (int) (Math.random() * 200 + 20);
		}

		TraficLight lamp = new TraficLight("Hijau", 0, 120, 1, 120);// lampu posisi jarum jam 12
		lamps.add(lamp);
		lamp = new TraficLight("Merah", 240, 0, 2, capacity[0]);// lampu posisi jarum jam 3
		lamps.add(lamp);
		lamp = new TraficLight("Merah", 360, 0, 3, capacity[1]);// lampu posisi jarum jam 6
		lamps.add(lamp);
		lamp = new TraficLight("Merah", 480, 0, 4, capacity[2]);// lampu posisi jarum jam 9
		lamps.add(lamp);
		lamp.changeColorJammed(lamps);
		for (int i = 0; i < lamps.size(); i++) {
			System.out.println(
					"Jalur " + lamps.get(i).getPosition() + " Jumlah Kendaraan : " + lamps.get(i).getCapacity());
		}
		lamps.get(0).start();
		lamps.get(1).start();
		try {
			lamps.get(1).sleep(10);
			lamps.get(2).start();
			lamps.get(2).sleep(10);
			lamps.get(3).start();
			lamps.get(3).sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
